// importing the "http" module
const http = require("http");

// storing the 400 in a variable called port
const port = 4000;

// Mock Database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
];

// storing the createServer method inside the server variable
const server = http.createServer((request, response) => {

	// route for returning all items upon receiving a GET method request
	if (request.url === "/users" && request.method === "GET" ) {
		response.writeHead(200, { "Content-Type": "application/json" });
		response.write(JSON.stringify(directory));
		response.end();
	};

	if (request.url === "/users" && request.method === "POST" ) {
		let requestBody = "";
		// 
		// A stream is a sequence of data

		// data is received from the client and is processed in the "data" stream
		// the information provided from the request object enters a sequence called "data" the code below will be triggered
			// "data" step - this reads the "data" stream and processes it as the request body



		request.on("data", function(data){
			// assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		});
			// request end step - only runs after the request has completely been sent
			request.on("end", function(){

				// checks if at this point, the requestBody if off data type STRING

				// we need this to be of data type JSON to access its properties
				console.log(typeof requestBody);

				// converts the string requestBody to JSON
				requestBody = JSON.parse(requestBody);

				// creating new object representing the new mock database record
				let newUser = {
					"name": requestBody.name,
					"email": requestBody.email
				};

				// Adds the new user into the mock database
				directory.push(newUser);
				console.log(directory);

				// data from database , back to the server, back to the client
				// after the interaction with database
				// after processing the request, whether there's error or none (any response)
				response.writeHead(200, {"Content-Type": "application/json"});
				response.write(JSON.stringify(newUser));
				response.end();
			});
	};
});

// using server and port variables
server.listen(port);

// confirmation that the server is running
console.log(`Server now running at port: ${port}`);

// postman - for users input
// postman - serves as our front-end
// testing of api, if it will work if we have front-end side