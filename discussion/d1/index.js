// importing the "http" module
const http = require("http");

// storing the 400 in a variable called port

const port = 4000;

// storing the createServer method inside the server variable
// const server = http.createServer((request, response) => {
//   if (request.url === "/items") {
// 	response.writeHead(200, {"Content-Type": "text/plain"} );
// 	response.end("Data retrieved from the database");
//   }
// });


const server = http.createServer((request, response) => {

// HTTP method can be accesses via "method" property inside the "request" object; they are to be written in ALL CAPS

// GET REQUEST

  if (request.url === "/items" && request.method === "GET") {
	response.writeHead(200, {"Content-Type": "text/plain"} );
	response.end("Data retrieved from the database");
  };

// POST REQUEST
// "POST" means that we will be adding/creating information
// new user, new product, new service

  if (request.url === "/items" && request.method === "POST") {
  response.writeHead(200, {"Content-Type": "text/plain"} );
	response.end("Data to be sent to the database");

}});

// GET method is one of the HTTPS method that we will be using from this point
  // GET method means that we will be retrieving or reading information


  // / using server and port variables
server.listen(port);

// confirmation that the server is running
console.log(`Server now running at port: ${port}`);

// npx kill-port <port> 
// can be used to kill